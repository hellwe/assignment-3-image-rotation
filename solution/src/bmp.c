#include "../include/bmp.h"

struct bmp_header create_header(uint64_t width, uint64_t height) {
    uint64_t img_size = (uint32_t)(sizeof(struct pixel)*width + (4 - (width * sizeof(struct pixel))) % 4)*(uint32_t)height;
    struct bmp_header header;
    header.bfType = type;
    header.bfileSize = (uint32_t)sizeof(struct bmp_header) + img_size;
    header.bfReserved = paramDefault;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = info_size;
    header.biWidth = (uint32_t)width;
    header.biHeight = (uint32_t)height;
    header.biPlanes = planes;
    header.biBitCount = bitCount;
    header.biCompression = paramDefault;
    header.biSizeImage = img_size;
    header.biXPelsPerMeter = paramDefault;
    header.biYPelsPerMeter = paramDefault;
    header.biClrUsed = paramDefault;
    header.biClrImportant = paramDefault;
    return header;

}

static inline uint8_t calculate_padding(uint64_t width) {
    return (4 - (width * sizeof(struct pixel))) % 4;
}

enum write_status to_bmp( FILE* out, struct image const* image ){
    uint64_t width = image -> width;
    uint64_t height = image -> height;

    size_t padding = calculate_padding(image->width);

    struct bmp_header header = create_header(width, height);

    if (!fwrite(&header, sizeof (struct bmp_header), 1, out)) return WRITE_ERROR;

    size_t buf = 0;
    
    for (uint32_t i = 0; i < image->height; i++){
        size_t width_2 = fwrite(image->data + i * image->width, sizeof(struct pixel), width, out);
        if (width_2 != width) return WRITE_ERROR;
        if(!fwrite(&buf, padding, 1, out)) return WRITE_ERROR;
    }
    return WRITE_OK;

}

enum read_status from_bmp( FILE* in, struct image* image ){
    if (in == NULL || image == NULL){return READ_INVALID_BITS;}
    struct bmp_header header;
    if (fread(&header, header_size, 1, in) != 1){return READ_INVALID_HEADER;}
    *image = create_image((uint64_t)header.biWidth, header.biHeight);
    if(image->width*image->height==0){
        destroy_image(image);
        return READ_INVALID_HEADER;
    }
    for (uint32_t y=0; y<image->height; y++){
        if(fread(get_pixel(0, y, image), sizeof(struct pixel), image->width, in) != image->width || (fseek(in, calculate_padding(image->width), SEEK_CUR) != 0)){
            destroy_image(image);
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;

}   


#include "../include/bmp.h"
#include "../include/image.h"
#include "../include/rotate.h"
#include <stdio.h>
#include <stdlib.h>



int main(int argc, char** argv) {

    if (argc != 3) 
    {
        puts("Require args may be only 3\n");
        return EXIT_FAILURE;
    }

    FILE *input = fopen(argv[1], "rb");
    if (input == NULL) return EXIT_FAILURE;

    struct image image = {0};

    enum read_status read_status = from_bmp(input, &image);
     if (read_status != READ_OK) {
        fprintf(stderr,"Read error\n");
        return EXIT_FAILURE;
        }
    if (fclose(input) != 0){
        fprintf(stderr,"Input error\n");
        return EXIT_FAILURE;
    }
        struct image rotated = rotate(&image);
        destroy_image(&image);
        puts("Successfully rotated! \n");
        FILE *output = fopen(argv[2], "wb");
        enum write_status write_status = to_bmp(output, &rotated);
        if(output == NULL){
            fprintf(stderr,"No output\n");
            return EXIT_FAILURE;
        }
        if(write_status != WRITE_OK){
            fprintf(stderr,"Write error\n");
            return EXIT_FAILURE;
        }
        if(fclose(output) != 0){
            fprintf(stderr,"Output error\n");
            return EXIT_FAILURE;
        }
        
        destroy_image(&rotated);
        return EXIT_SUCCESS;
        
}
